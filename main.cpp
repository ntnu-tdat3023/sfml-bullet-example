#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/System.hpp>

#ifdef __APPLE__
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif

#include <glm/glm.hpp>

#include <btBulletDynamicsCommon.h>

#include <chrono>

class Ground {
  btStaticPlaneShape shape;
  btDefaultMotionState motion_state;

public:
  btRigidBody body;

  Ground() : shape(btVector3(0.0, 1.0, 0.0), 0.0), body(0.0, &motion_state, &shape) {
    body.setRollingFriction(0.1);
    body.setRestitution(0.8);
  }

  void draw() {
    glBegin(GL_TRIANGLE_STRIP);
    glColor3f(0.0, 1.0, 0.0);
    glVertex3f(-2.0, 0.0, 2.0);
    glVertex3f(-2.0, 0.0, -2.0);
    glVertex3f(2.0, 0.0, 2.0);
    glVertex3f(2.0, 0.0, -2.0);
    glEnd();
  }
};

class Sphere {
  btSphereShape shape;
  btDefaultMotionState motion_state;

public:
  btRigidBody body;

  Sphere() : shape(btScalar(0.1)), body(1.0, &motion_state, &shape) {
    auto mass = 1.0 / body.getInvMass();

    btVector3 inertia;
    shape.calculateLocalInertia(mass, inertia);

    body.setMassProps(mass, inertia);
    body.setRollingFriction(0.1);
    body.setRestitution(0.8);
  }

  void draw() {
    auto quadric = gluNewQuadric();
    auto position = body.getCenterOfMassPosition();

    glPushMatrix();
    glTranslatef(position.x(), position.y(), position.z());
    glColor3f(0.0, 0.0, 1.0);
    gluSphere(quadric, shape.getRadius(), 32, 32);
    glPopMatrix();

    gluDeleteQuadric(quadric);
  }
};

class World {
  btDefaultCollisionConfiguration collision_configuration;
  btCollisionDispatcher dispatcher;
  btDbvtBroadphase broadphase;
  btSequentialImpulseConstraintSolver solver;

public:
  btDiscreteDynamicsWorld dynamics;

  Ground ground;
  Sphere sphere1, sphere2;

  World() : dispatcher(&collision_configuration), dynamics(&dispatcher, &broadphase, &solver, &collision_configuration) {
    dynamics.setGravity(btVector3(0, -10.0, 0));

    //Add objects to the physics engine
    dynamics.addRigidBody(&ground.body);
    dynamics.addRigidBody(&sphere1.body);
    dynamics.addRigidBody(&sphere2.body);

    btTransform transform;

    //Position ground
    transform.setIdentity();
    transform.setOrigin(btVector3(0.0, 0.0, 0.0));
    ground.body.setCenterOfMassTransform(transform);

    //Position spheres
    transform.setIdentity();
    transform.setOrigin(btVector3(-2.0, 2.0, 0.0));
    sphere1.body.setCenterOfMassTransform(transform);

    transform.setIdentity();
    transform.setOrigin(btVector3(2.0, 2.0, 0.0));
    sphere2.body.setCenterOfMassTransform(transform);

    //Set initial sphere velocities
    sphere1.body.setLinearVelocity(btVector3(2.0, 0.0, 0.0));
    sphere2.body.setLinearVelocity(btVector3(-2.0, 0.0, 0.0));
  }

  void draw() {
    ground.draw();
    sphere1.draw();
    sphere2.draw();
  }
};

class SFMLApplication {
  sf::ContextSettings context_settings;
  sf::Window window;

  World world;

public:
  SFMLApplication() : context_settings(24),
                      window(sf::VideoMode(800, 600), "SFML Example", sf::Style::Default, context_settings) {
    window.setFramerateLimit(144);
    window.setVerticalSyncEnabled(true);

    //Various settings
    glClearColor(0.5, 0.5, 0.5, 0.0);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    //Setup projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    gluPerspective(45.0, 4.0 / 3.0, 0.1, 100.0);

    glMatrixMode(GL_MODELVIEW);
  }

  void start() {
    glm::vec3 camera(0.0, 1.0, 6.0);

    auto last_time = std::chrono::system_clock::now();
    bool running = true;
    while (running) {
      //Handle events
      sf::Event event;
      while (window.pollEvent(event)) {
        if (event.type == sf::Event::KeyPressed) {
          if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            window.close();
            running = false;
          }
        }
        else if (event.type == sf::Event::MouseMoved) {
          camera.x = 0.01 * -(event.mouseMove.x - static_cast<int>(window.getSize().x) / 2);
          camera.y = 0.01 * (event.mouseMove.y - static_cast<int>(window.getSize().y) / 2);
        }
        else if (event.type == sf::Event::Closed) {
          window.close();
          running = false;
        }
      }

      //Draw
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glLoadIdentity();

      gluLookAt(camera.x, camera.y, camera.z, //Camera position in World Space
                camera.x, camera.y, 0.0,      //Camera looks towards this position
                0.0, 1.0, 0.0);               //Up

      auto time = std::chrono::system_clock::now();
      world.dynamics.stepSimulation(std::chrono::duration<float>(time - last_time).count());
      last_time = time;

      world.draw();

      //Swap buffer (show result)
      window.display();
    }
  }
};

int main() {
  SFMLApplication sfml_application;
  sfml_application.start();

  return 0;
}
