# Example SFML application with Bullet Physics and older style OpenGL

## Prerequisites
The C++ IDE [juCi++](https://github.com/cppit/jucipp) should be installed.

## Installing dependencies

### Debian based distributions
`sudo apt-get install libsfml-dev libglm-dev libbullet-dev`

### Arch Linux based distributions
`sudo pacman -S sfml glm bullet`

### OS X
`brew install sfml glm bullet`

## Compiling and running
```sh
git clone https://gitlab.com/ntnu-tdat3023/sfml-bullet-example
juci sfml-bullet-example
```

Choose Compile and Run in the Project menu.
